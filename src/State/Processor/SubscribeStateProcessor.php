<?php

namespace App\State\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Core\ApiRoute\SubscribeRoute;
use App\Entity\Subscribe;
use App\Service\Payment\SubscribeService;
use Stripe\Exception\ApiErrorException;

readonly class SubscribeStateProcessor implements ProcessorInterface
{

    public function __construct(private SubscribeService $service)
    {
    }

    /**
     * @param Subscribe $data
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return Subscribe|mixed
     * @throws ApiErrorException
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // TODO: Implement process() method.
        if ($operation->getName() == SubscribeRoute::POST['name']) {
            $this->service->subscribe($data);
        }

        $data->setId(10);

        return $data;
    }
}