<?php

namespace App\State\Processor;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Core\Route\UserRoute;
use App\Core\Useful\AuthType;
use App\Entity\Auth;
use App\Entity\Email;
use App\Entity\Phone;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

readonly class UserStateProcessor implements ProcessorInterface
{

    public function __construct(
        private EntityManagerInterface      $em,
        private UserPasswordHasherInterface $passwordEncoder,
    )
    {
    }

    /** @param User $data
     * @throws Exception
     */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        // TODO: Implement process() method.

        if($operation->getName() === UserRoute::ADD['name'])
        {
            if (!$data->getIdentifier()) throw new Exception('You must provide a valid identifier');

            $authType = filter_var($data->getIdentifier(), FILTER_VALIDATE_EMAIL) ? AuthType::EMAIL : AuthType::PHONE;

            $repository = $authType === AuthType::EMAIL ? $this->em->getRepository(Email::class) : $this->em->getRepository(Phone::class);

            $authEntity = $repository->findOneBy(['value' => $data->getIdentifier()]);

            if ($authEntity) throw new Exception('A user with this ID already exists in our system');

            $authEntity = ($authType === AuthType::EMAIL ? new Email() : new Phone())
                ->setValue($data->getIdentifier())->setActive(true)->setOwner($data);
            ;

            $auth = (new Auth())->setType($authType);

            $authEntity->setAuth($auth);

            if ($authType === AuthType::EMAIL) $auth->setEmail($authEntity);
            else $auth->setPhone($authEntity);

            $this->em->persist($authEntity);
            $this->em->persist($auth);
        }

        if($data->getPlainPassword()) {
            $data->setPassword($this->passwordEncoder->hashPassword($data, $data->getPlainPassword()));
            $data->eraseCredentials();
        }

        $this->em->persist($data);
        $this->em->flush();

        return $data;
    }
}