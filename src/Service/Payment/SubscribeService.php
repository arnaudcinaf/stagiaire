<?php

namespace App\Service\Payment;

use App\Core\Entity\WebhookResponse;
use App\Core\Useful\Strategy;
use App\Core\Useful\SubscriptionState;
use App\Core\Useful\TransactionMessage;
use App\Core\Useful\TransactionStatus;
use App\Core\Useful\WebhookAction;
use App\Core\Useful\WebhookType;
use App\Entity\Aggregator;
use App\Entity\Payment;
use App\Entity\Subscribe;
use App\Entity\Subscription;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\Webhook;
use App\Repository\SubscriptionRepository;
use App\Repository\WebhookRepository;
use App\Service\Payment\Aggregator\StripeAggregatorService;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\SignatureVerificationException;
use Symfony\Bundle\SecurityBundle\Security;

class SubscribeService
{
    private User $currentUser;
    private WebhookResponse $webhookResponse;
    private Client $client;
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly StripeAggregatorService $stripeAggregatorService,
        private readonly Security $security
    )
    {
        /** @var User $user*/
        $user = $this->security->getUser();

        $this->currentUser = $user;

        $this->client = new Client();
    }

    /**
     * @throws ApiErrorException
     */
    public function subscribe(Subscribe &$subscribe): void
    {
        $subscribe->setOwner($this->currentUser);

        $date = new DateTimeImmutable();

        $methodPaid = $subscribe->getMethodPaid();

        $strategy = $methodPaid->getAggregator()->getStrategy();

        $transaction = (new Transaction())
        ->setOwner($this->currentUser)
            ->setStatus(TransactionStatus::INIT)
            ->setAmount($subscribe->getPackage()->getPrice())
            ->setCreatedAt($date)
            ->setUpdatedAt($date)
        ;

        $this->entityManager->persist($transaction);
        $this->entityManager->flush();

        $subscribe->setTransaction($transaction);

        if ($strategy == Strategy::STRIPE) {
            $this->stripeAggregatorService->payment($subscribe);
        }

        $webhook = (new Webhook())
            ->setTransaction($transaction)
            ->setSession($subscribe->getSession())
            ->setPackage($subscribe->getPackage())
            ->setMethodPaid($methodPaid)
        ;

        $this->entityManager->persist($webhook);
        $this->entityManager->flush();
    }

    /**
     * @throws SignatureVerificationException
     * @throws ApiErrorException
     * @throws Exception
     */
    public function handleWebhook(string $strategy): void
    {
        $repository = $this->entityManager->getRepository(Aggregator::class);

        $aggregator = $repository->findOneBy(['strategy' => $strategy]);

        if ($strategy == Strategy::STRIPE) {
            $this->webhookResponse = $this->stripeAggregatorService->webhookListener($aggregator);
        }

        $this->webhookWorker();

    }

    /**
     * @throws Exception
     */
    private function webhookWorker():void
    {
        switch ($this->webhookResponse->getAction())
        {
            case WebhookAction::POST:

                $this->webhookResponse->getType() === WebhookType::RECURRENT ?
                    $this->postRecurrentSubscription() :
                    $this->postPunctualSubscription();

                break;

            case WebhookAction::PATCH:

                $this->patchSubscription();

                break;

        }
    }

    /**
     * @return void
     *@throws Exception
     */
    private function postPunctualSubscription():void
    {
        if (!$this->webhookResponse->getTransaction()) {
            return;
        }

        $date = new DateTimeImmutable();

        /** @var WebhookRepository $webhookRepo*/
        $webhookRepo = $this->entityManager->getRepository(Webhook::class);

        $webhook = $this->defaultWebhook ?? $webhookRepo
            ->findBySession($this->webhookResponse->getTransaction());

        if($webhook) {

            $transaction = $webhook->getTransaction();

            $transaction
                ->setMessage(TransactionMessage::SUCCESS_SUBSCRIPTION)
                ->setStatus(TransactionStatus::SUCCEEDED);

            $persistWebhook = false;

            if((string) $transaction->getId() !== $webhook->getSession()) {

                $webhook->setSession((string) $transaction->getId());

                $persistWebhook = true;
            }

            if ($this->webhookResponse->getSubscription()) {

                $webhook->setSubId($this->webhookResponse->getSubscription());

                $persistWebhook = true;
            }

            $package = $webhook->getPackage();
            $duration = $package->getDuration();

            $subscription = (new Subscription())
                ->setPackage($package)
                ->setOwner($transaction->getOwner())
                ->setMethodPaid($webhook->getMethodPaid())
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ->setSubId($webhook->getSubId())
                ->setState(SubscriptionState::ACTIVATE)
            ;

            $payment = (new Payment())
                ->setSubscription($subscription)
                ->setStartAt($date)
                ->setFinishAt($date->modify("+$duration months"))
            ;

            $this->entityManager->persist($subscription);
            $this->entityManager->persist($payment);
            $this->entityManager->persist($transaction);
            if ($persistWebhook) $this->entityManager->persist($webhook);

            $this->entityManager->flush();

            try {
                $this->notifyTransactionUpdate($transaction->getId(), ['subscription' => $subscription->getId()]);
            }
            catch (Exception $exception) {}
        }

    }

    /**
     * @return void
     * @throws Exception
     */
    private function postRecurrentSubscription (): void
    {
        /** @var SubscriptionRepository $subscriptionRepo*/
        $subscriptionRepo = $this->entityManager->getRepository(Subscription::class);

        $sub = $subscriptionRepo
            ->findBySubId($this->webhookResponse->getSubscription());

        $sub ? $this->handleResolveRecurrentPayment($sub) : $this->postPunctualSubscription();

    }

    /**
     * @throws Exception
     */
    private function handleResolveRecurrentPayment(Subscription $sub): void
    {
        if(!$sub->getSubId()) {
            return;
        }

        $paymentRepository = $this->entityManager->getRepository(Payment::class);

        $oldPayment = $paymentRepository->findOneBy(['subscription' => $sub->getId()], ['finishAt' => 'DESC']);

        $date = new DateTimeImmutable();

        $start = $oldPayment->getFinishAt();
        $package = $sub->getPackage();
        $duration = $package->getDuration();

        $payment = (new Payment())->setSubscription($sub)->setStartAt($start)->setFinishAt($start->modify("+$duration months"));

        $sub->setUpdatedAt($date);

        $transaction = (new Transaction())
            ->setUpdatedAt($date)
            ->setOwner($sub->getOwner())
            ->setCreatedAt($date)
            ->setAmount($package->getPrice())->setStatus(TransactionStatus::SUCCEEDED)->setMessage(TransactionMessage::SUCCESS_RENEWABLE);

        $this->entityManager->persist($sub);
        $this->entityManager->persist($payment);
        $this->entityManager->persist($transaction);

        $this->entityManager->flush();

        try {
            $this->notifyTransactionUpdate($transaction->getId(), ['subscription' => $sub->getId()]);
        } catch (Exception $exception){}
    }

    private function patchSubscription(): void
    {
    }

    public function notifyTransactionUpdate($transactionId, $data): void
    {
        $baseUrl = $_ENV['SOCKET_URL'];
        try {
            $this->client->post("$baseUrl/publish/$transactionId", [
                'json' => $data
            ]);
        } catch (GuzzleException $e) {

        }
    }
}