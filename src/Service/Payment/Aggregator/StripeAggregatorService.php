<?php

namespace App\Service\Payment\Aggregator;

use App\Core\Entity\WebhookResponse;
use App\Core\Useful\StripeEvent;
use App\Core\Useful\StripeStatus;
use App\Core\Useful\SubscriptionState;
use App\Core\Useful\WebhookAction;
use App\Core\Useful\WebhookType;
use App\Entity\Aggregator;
use App\Entity\Customer;
use App\Entity\Price;
use App\Entity\Subscribe;
use App\Entity\Subscription;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Stripe\Checkout\Session;
use Stripe\Event;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\SignatureVerificationException;
use Stripe\Invoice;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\Subscription as StripeSubscription;
use Stripe\Webhook;
use Symfony\Component\HttpFoundation\RequestStack;

class StripeAggregatorService
{
    private Customer $customer;
    private WebhookResponse $webhookResponse;
    private bool $devMode = false;
    private array $methods = ['card','bancontact','ideal','sofort', 'sepa_debit'];
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly RequestStack $requestStack
    )
    {
        $this->devMode = $_ENV['APP_ENV'] === 'dev';
    }

    private function load(Aggregator $aggregator): void
    {
        $apiKey = $this->devMode ? $aggregator->getDevApiKey()  : $aggregator->getApiKey();

        Stripe::setApiKey($apiKey);
        Stripe::setApiVersion($aggregator->getVersion());
    }

    /**
     * @throws ApiErrorException
     */
    public function payment(Subscribe &$subscribe): void
    {
        $this->load($subscribe->getMethodPaid()->getAggregator());

        $this->createCustomId($subscribe);

        $repository = $this->entityManager->getRepository(Price::class);

        $packagePrice = $repository->findOneBy(['package' => $subscribe->getPackage()->getId(), 'aggregator' => $subscribe->getMethodPaid()->getAggregator()->getId()]);
        $mode = $packagePrice->isRecurrent() ? WebhookType::RECURRENT : WebhookType::PUNCTUAL;
        $session = Session::create(
            [
                'payment_method_types' => $packagePrice->isRecurrent() ? $this->methods :
                    array_merge($this->methods, ['eps', 'giropay', 'klarna', 'p24']),
                'mode' => $mode,
                'customer' => $this->customer->getValue(),
                'line_items' => [[
                    'price' => $this->devMode ? $packagePrice->getDevValue() : $packagePrice->getValue(),
                    'quantity' => 1
                ]],
                'automatic_tax' => [
                    'enabled' => false,
                ],
                'expires_at' => time() + 3600,
                'customer_update' => [
                    'address' => 'auto',
                    'shipping' => 'auto',
                ],
                'success_url' => trim($subscribe->getRedirectUrl()).'/payment/'.
                    ($subscribe->getTransaction()->getId()).'?payment_status=success_payment',
                'cancel_url' =>  trim($subscribe->getRedirectUrl()).'/payment/'.
                    ($subscribe->getTransaction()->getId()).'?payment_status=cancel_payment'
            ]
        );

        $subscribe->setRedirectUrl($session->url)->setSession($this->customer->getValue())->setSession((string) $subscribe->getTransaction()->getId());
    }

    /**
     * @throws SignatureVerificationException
     * @throws ApiErrorException
     * @throws Exception
     */
    public function webhookListener(Aggregator $aggregator): WebhookResponse
    {
        $this->load($aggregator);

        $request = $this->requestStack->getMainRequest();

        $signature = $request->headers->get('Stripe-Signature');
        $body = $request->getContent();

        $secret = $aggregator->getWebhookKey();

        $event = Webhook::constructEvent(
            $body,
            $signature,
            $secret
        );

        $this->webhookResponse = (new WebhookResponse())->setAction(WebhookAction::VOID);



        switch ($event->type)
        {
            case StripeEvent::INVOICE_PAID:

                $this->invoicePaid($event);

                break;

            case StripeEvent::INTENT_SUCCEEDED:

                $this->intentSucceeded($event);

                break;

            case StripeEvent::CUSTOMER_SUBSCRIPTION_UPDATED:

                $this->customerSubscriptionUpdate($event);

                break;
        }

        return $this->webhookResponse;
    }

    /**
     * @throws ApiErrorException
     */
    private function createCustomId(Subscribe $subscribe): void
    {
        $repository = $this->entityManager->getRepository(Customer::class);

        $date = new DateTimeImmutable();

        $ip_address = $this->devMode ?
            '145.224.74.198' :
            $this->requestStack->getMainRequest()->getClientIp();

        $clientCustomer = $repository->findOneBy(
            [
                'owner'=> $subscribe->getOwner()->getId(),
                'aggregator' => $subscribe->getMethodPaid()->getAggregator()->getId()
            ]
        );

        if (!$clientCustomer) {
            $currentUser = $subscribe->getOwner();
            $params = [
                'email' => $currentUser->getDefaultEmail(),
                'name' => implode(' ', [$currentUser->getLastName(), $currentUser->getFirstName()]),
                'tax' => ['ip_address' => $ip_address],
                'expand' => ['tax'],
            ];
            $customer = \Stripe\Customer::create($params);

            $clientCustomer = (new Customer())
                ->setOwner($currentUser)
                ->setAggregator($subscribe->getMethodPaid()->getAggregator())
                ->setValue($customer->id)
                ->setCreatedAt($date)
                ->setUpdatedAt($date)
                ;

            $this->entityManager->persist($clientCustomer);

            $this->entityManager->flush();
        }
        else
        {
            \Stripe\Customer::update(
                $clientCustomer->getValue(),
                [
                    'tax' => ['ip_address' => $ip_address],
                    'expand' => ['tax'],
                ]
            );
        }

        $this->customer = $clientCustomer;
    }

    /**
     * @throws ApiErrorException
     */
    public function toggleCancelSubscription(Subscription $subscription): bool
    {
        $sub = StripeSubscription::update(
            $subscription->getSubId(),
            [
                'cancel_at_period_end' => $subscription->getState() !== SubscriptionState::CANCELED,
            ]
        );

        return $sub->cancel_at_period_end === $subscription->isAsset() && $sub->status === StripeStatus::ACTIVE;
    }

    /**
     * @throws ApiErrorException
     */
    public function toggleSuspendSubscription(Subscription $subscription): bool
    {
        return $subscription->getState() === SubscriptionState::SUSPENDED ?
            $this->pauseStart($subscription) :
            $this->pauseStop($subscription);
    }

    /**
     * @throws Exception
     */
    private function invoicePaid (Event $event): void
    {
        $data = $event->data['object'];

        if($data['status'] === StripeStatus::PAID) {

            $invoice = Invoice::retrieve($data['id']);

            $this->webhookResponse
                ->setSubscription($invoice->subscription)
                ->setTransaction($invoice->customer)
                ->setState(SubscriptionState::ACTIVATED)
                ->setAction(\App\Useful\Entity\Webhook\WebhookAction::POST)
                ->setType(WebhookType::RECURRENT)
            ;
        }
    }

    /**
     * @throws Exception
     */
    private function intentSucceeded (Event $event): void
    {
        $data = $event->data['object'];

        if ($this->matchingPaymentIntent($data) && $data['status'] === StripeStatus::SUCCEEDED) {
            $paymentIntent = PaymentIntent::retrieve($data['id']);
            $this->webhookResponse
                ->setTransaction($paymentIntent->customer)
                ->setState(SubscriptionState::ACTIVATED)
                ->setAction(WebhookAction::POST)
                ->setType(WebhookType::PUNCTUAL)
            ;
        }

    }

    /**
     * @throws ApiErrorException
     */
    private function customerSubscriptionUpdate(Event $event):void
    {
        $data = $event->data['object'];

        if ($data['status'] === StripeStatus::ACTIVE)
        {
            $subscription = StripeSubscription::retrieve($data['id']);

            $status = $this->isPause($subscription) ? SubscriptionState::SUSPENDED :
                ($this->isCancel($subscription) ? SubscriptionState::CANCELED : SubscriptionState::ACTIVATED);

            $this->webhookResponse
                ->setSubscription($subscription->id)
                ->setState($status)
                ->setAction(WebhookAction::PATCH)
            ;

        }
    }

    /**
     * @throws ApiErrorException
     */
    private function pauseStart(Subscription $subscription):bool
    {
        $sub = StripeSubscription::update(
            $subscription->getSubId(),
            [
                'pause_collection' => ['behavior' => 'void'],
            ]
        );

        return $this->isPause($sub);
    }



    /**
     * @throws ApiErrorException
     */
    private function pauseStop(Subscription $subscription):bool
    {
        $sub = StripeSubscription::update(
            $subscription->getSubId(),
            [
                'pause_collection' => '',
            ]
        );

        return !$this->isPause($sub);
    }

    private function isPause (StripeSubscription $subscription):bool
    {
        return $subscription->pause_collection !== null && $subscription->status === StripeStatus::ACTIVE;
    }

    private function isCancel (StripeSubscription $subscription):bool
    {
        return $subscription->cancel_at_period_end && $subscription->status === StripeStatus::ACTIVE;
    }
}