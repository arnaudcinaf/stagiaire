<?php

namespace App\Core\Helpers\OpenApi;

class Helpers
{
    const AUTH_USER = ['security' => [['bearerAuth' => []]]];

    const SUMMARY_HIDDEN = ['summary' => 'hidden'];
    const GRANTED_ADMIN = "is_granted('ROLE_ADMIN')";
    const GRANTED_USER = "is_granted('ROLE_USER')";
    const GRANTED_PUBLIC = "is_granted('PUBLIC_ACCESS')";
    const REQUIREMENT_ID = ['id'=> '\d+'];
    const REQUIREMENT_ID_UID = ['id'=> '\b(?!me\b)\w+\b'];
}