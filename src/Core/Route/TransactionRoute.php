<?php

namespace App\Core\Route;

class TransactionRoute
{
    const ITEM = ['path' => '/transactions/{id}', 'name' => 'transaction.item'];
    const COLLECTION = ['path' => '/transactions', 'name' => 'transaction.collection'];
    const ME_COLLECTION = ['path' => '/transactions/me', 'name' => 'transaction.me.collection'];
    const ADD = ['path' => '/transactions/add', 'name' => 'transaction.add'];
}