<?php

namespace App\Core\Route;

class PriceRoute
{
    const ITEM = ['path' => '/prices/{id}', 'name' => 'price.item'];
    const COLLECTION = ['path' => '/prices', 'name' => 'price.collection'];
    const ADD = ['path' => '/prices/add', 'name' => 'price.add'];
    const PATCH = ['path' => '/prices/{id}', 'name' => 'price.patch'];
}