<?php

namespace App\Core\Route;

class AggregatorRoute
{
    const ITEM = ['path' => '/aggregators/{id}', 'name' => 'aggregator.item'];
    const COLLECTION = ['path' => '/aggregators', 'name' => 'aggregator.collection'];
    const ADD = ['path' => '/aggregators/add', 'name' => 'aggregator.add'];
    const PATCH = ['path' => '/aggregators/{id}', 'name' => 'aggregator.patch'];
}