<?php

namespace App\Core\Route;

class UserRoute
{
    const ITEM = ['path' => '/users/{id}', 'name' => 'user.item'];
    const COLLECTION = ['path' => '/users', 'name' => 'user.collection'];
    const ME = ['path' => '/users/me', 'name' => 'user.me'];
    const ADD = ['path' => '/users/add', 'name' => 'user.add'];
    const PATCH = ['path' => '/users/{id}', 'name' => 'user.patch'];
}