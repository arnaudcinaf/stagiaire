<?php

namespace App\Core\Route;

class EmailRoute
{
    const ITEM = ['path' => '/emails/{id}', 'name' => 'email.item'];
    const COLLECTION = ['path' => '/emails', 'name' => 'email.collection'];
    const ME_COLLECTION = ['path' => '/emails/me', 'name' => 'email.me.collection'];
    const ADD = ['path' => '/emails/add', 'name' => 'email.add'];
}