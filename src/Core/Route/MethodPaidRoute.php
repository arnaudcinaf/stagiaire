<?php

namespace App\Core\Route;

class MethodPaidRoute
{
    const ITEM = ['path' => '/methodPaids/{id}', 'name' => 'methodPaid.item'];
    const COLLECTION = ['path' => '/methodPaids', 'name' => 'methodPaid.collection'];
    const ADD = ['path' => '/methodPaids/add', 'name' => 'methodPaid.add'];
    const PATCH = ['path' => '/methodPaids/{id}', 'name' => 'methodPaid.patch'];
}