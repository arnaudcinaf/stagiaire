<?php

namespace App\Core\Route;

class PackageRoute
{
    const ITEM = ['path' => '/packages/{id}', 'name' => 'package.item'];
    const COLLECTION = ['path' => '/packages', 'name' => 'package.collection'];
    const ADD = ['path' => '/packages/add', 'name' => 'package.add'];
    const PATCH = ['path' => '/packages/{id}', 'name' => 'package.patch'];
}