<?php

namespace App\Core\Route;

class SubscriptionRoute
{
    const ITEM = ['path' => '/subscriptions/{id}', 'name' => 'subscription.item'];
    const COLLECTION = ['path' => '/subscriptions', 'name' => 'subscription.collection'];
    const ME = ['path' => '/subscriptions/me', 'name' => 'subscription.me'];

    const ADD = ['path' => '/subscriptions/add', 'name' => 'subscription.add'];
}