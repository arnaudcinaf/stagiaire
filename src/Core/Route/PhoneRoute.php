<?php

namespace App\Core\Route;

class PhoneRoute
{
    const ITEM = ['path' => '/phones/{id}', 'name' => 'phone.item'];
    const COLLECTION = ['path' => '/phones', 'name' => 'phone.collection'];
    const ME_COLLECTION = ['path' => '/phones/me', 'name' => 'phone.me.collection'];
    const ADD = ['path' => '/phones/add', 'name' => 'phone.add'];
}