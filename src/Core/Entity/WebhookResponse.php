<?php

namespace App\Core\Entity;

class WebhookResponse
{
    private ?string $subscription = null;
    private ?string $transaction = null;
    private ?string $state = null;
    private ?string $action = null;
    private ?string $type = null;
    private ?string $session = null;

    /**
     * @return string|null
     */
    public function getSubscription(): ?string
    {
        return $this->subscription;
    }

    /**
     * @param string|null $subscription
     * @return WebhookResponse
     */
    public function setSubscription(?string $subscription): WebhookResponse
    {
        $this->subscription = $subscription;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTransaction(): ?string
    {
        return $this->transaction;
    }

    /**
     * @param string|null $transaction
     * @return WebhookResponse
     */
    public function setTransaction(?string $transaction): WebhookResponse
    {
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return WebhookResponse
     */
    public function setState(?string $state): WebhookResponse
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     * @return WebhookResponse
     */
    public function setAction(?string $action): WebhookResponse
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return WebhookResponse
     */
    public function setType(?string $type): WebhookResponse
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSession(): ?string
    {
        return $this->session;
    }

    /**
     * @param string|null $session
     * @return WebhookResponse
     */
    public function setSession(?string $session): WebhookResponse
    {
        $this->session = $session;
        return $this;
    }
}