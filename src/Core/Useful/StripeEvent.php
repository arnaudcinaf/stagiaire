<?php

namespace App\Core\Useful;

class StripeEvent
{
    const CHECKOUT_COMPLETED = 'checkout.session.completed';//checkout.session.expired
    const CHECKOUT_EXPIRED = 'checkout.session.expired';
    const INTENT_SUCCEEDED = 'payment_intent.succeeded';
    const INVOICE_SUCCEEDED = 'invoice.payment_succeeded';
    const INVOICE_FAILED = 'invoice.payment_failed';
    const INVOICE_PAID = 'invoice.paid';
    const CUSTOMER_SUBSCRIPTION_UPDATED = 'customer.subscription.updated';
}