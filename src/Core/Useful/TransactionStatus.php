<?php

namespace App\Core\Useful;

class TransactionStatus
{
    const INIT = 'init';
    const PENDING = 'pending';
    const PROGRESS = 'in_progress';
    const FAILED = 'failed';
    const SUCCEEDED = 'succeeded';
    const CANCELED = 'canceled';
}