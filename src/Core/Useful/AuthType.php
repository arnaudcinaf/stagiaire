<?php

namespace App\Core\Useful;

class AuthType
{
    const EMAIL = 'email';
    const PHONE = 'phone';
}