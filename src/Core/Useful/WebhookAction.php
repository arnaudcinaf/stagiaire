<?php

namespace App\Core\Useful;

class WebhookAction
{
    const POST = 'POST';
    const PATCH = 'PATCH';
    const VOID = 'VOID';
}