<?php

namespace App\Core\Useful;

class SubscriptionState
{
    const ACTIVATED = 'ACTIVATED';
    const ACTIVATE = 'ACTIVE';

    const CANCELED = 'CANCELED';
    const SUSPENDED = 'SUSPENDED';
    const TRIALING = 'TRIALING';
}