<?php

namespace App\Core\Useful;

class StripeStatus
{
    const PAID = 'paid';
    const COMPLETE = 'complete';
    const SUCCEEDED = 'succeeded';

    const ACTIVE = 'active';
}