<?php

namespace App\Core\Useful;

class WebhookType
{
    const PUNCTUAL = 'payment';
    const RECURRENT = 'subscription';
}