<?php

namespace App\Core\Useful;

class Strategy
{
    const STRIPE = 'ST';
    const APPLE = 'AP';
    const GOOGLE = 'GP';
}