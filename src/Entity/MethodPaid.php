<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Core\Helpers\OpenApi\Helpers;
use App\Core\Route\MethodPaidRoute;
use App\Repository\MethodPaidRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MethodPaidRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: MethodPaidRoute::ITEM['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: MethodPaidRoute::ITEM['name']
        ),
        new GetCollection(
            uriTemplate: MethodPaidRoute::COLLECTION['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: MethodPaidRoute::COLLECTION['name']
        ),
        new Post(
            uriTemplate: MethodPaidRoute::ADD['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            security: Helpers::GRANTED_ADMIN,
            name: MethodPaidRoute::ADD['name']
        ),
        new Patch(
            uriTemplate: MethodPaidRoute::PATCH['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            security: Helpers::GRANTED_ADMIN,
            name: MethodPaidRoute::PATCH['name']
        )
    ]
)]
class MethodPaid
{
    const GROUP_READ = 'method_paid.read';
    const GROUP_WRITE = 'method_paid.read';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'methodPaids')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?Aggregator $aggregator = null;

    #[ORM\Column]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?DateTimeImmutable $updatedAt = null;

    /**
     * @var Collection<int, Subscription>
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'methodPaid')]
    private Collection $subscriptions;

    /**
     * @var Collection<int, Webhook>
     */
    #[ORM\OneToMany(targetEntity: Webhook::class, mappedBy: 'methodPaid')]
    private Collection $webhooks;

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->webhooks = new ArrayCollection();

        $date = new DateTimeImmutable();

        $this->setCreatedAt($date)->setUpdatedAt($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getAggregator(): ?Aggregator
    {
        return $this->aggregator;
    }

    public function setAggregator(?Aggregator $aggregator): static
    {
        $this->aggregator = $aggregator;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setMethodPaid($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getMethodPaid() === $this) {
                $subscription->setMethodPaid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Webhook>
     */
    public function getWebhooks(): Collection
    {
        return $this->webhooks;
    }

    public function addWebhook(Webhook $webhook): static
    {
        if (!$this->webhooks->contains($webhook)) {
            $this->webhooks->add($webhook);
            $webhook->setMethodPaid($this);
        }

        return $this;
    }

    public function removeWebhook(Webhook $webhook): static
    {
        if ($this->webhooks->removeElement($webhook)) {
            // set the owning side to null (unless already changed)
            if ($webhook->getMethodPaid() === $this) {
                $webhook->setMethodPaid(null);
            }
        }

        return $this;
    }
}
