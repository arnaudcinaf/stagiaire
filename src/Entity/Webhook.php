<?php

namespace App\Entity;

use App\Repository\WebhookRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WebhookRepository::class)]
class Webhook
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $session = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subId = null;

    #[ORM\OneToOne(inversedBy: 'webhook', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Transaction $transaction = null;

    #[ORM\ManyToOne(inversedBy: 'webhooks')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Package $package = null;

    #[ORM\ManyToOne(inversedBy: 'webhooks')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MethodPaid $methodPaid = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSession(): ?string
    {
        return $this->session;
    }

    public function setSession(string $session): static
    {
        $this->session = $session;

        return $this;
    }

    public function getSubId(): ?string
    {
        return $this->subId;
    }

    public function setSubId(string $subId): static
    {
        $this->subId = $subId;

        return $this;
    }

    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    public function setTransaction(Transaction $transaction): static
    {
        $this->transaction = $transaction;

        return $this;
    }

    public function getPackage(): ?Package
    {
        return $this->package;
    }

    public function setPackage(?Package $package): static
    {
        $this->package = $package;

        return $this;
    }

    public function getMethodPaid(): ?MethodPaid
    {
        return $this->methodPaid;
    }

    public function setMethodPaid(?MethodPaid $methodPaid): static
    {
        $this->methodPaid = $methodPaid;

        return $this;
    }
}
