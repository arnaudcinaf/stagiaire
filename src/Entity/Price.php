<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Core\Helpers\OpenApi\Helpers;
use App\Core\Route\PriceRoute;
use App\Repository\PriceRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PriceRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: PriceRoute::ITEM['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: PriceRoute::ITEM['name']
        ),
        new GetCollection(
            uriTemplate: PriceRoute::COLLECTION['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: PriceRoute::COLLECTION['name']
        ),
        new Post(
            uriTemplate: PriceRoute::ADD['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            name: PriceRoute::ADD['name']
        ),
        new Patch(
            uriTemplate: PriceRoute::PATCH['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            name: PriceRoute::PATCH['name']
        )
    ],
    security: Helpers::GRANTED_ADMIN
)]
class Price
{
    const GROUP_READ = 'price_read';
    const GROUP_WRITE = 'price_write';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?string $devValue = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?string $value = null;

    #[ORM\ManyToOne(inversedBy: 'prices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?Package $package = null;

    #[ORM\Column]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?DateTimeImmutable $updatedAt = null;

    #[ORM\ManyToOne(inversedBy: 'prices')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?Aggregator $aggregator = null;

    #[ORM\Column]
    private ?bool $recurrent = null;

    public function __construct()
    {
        $date = new DateTimeImmutable();

        $this->setCreatedAt($date)->setUpdatedAt($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDevValue(): ?string
    {
        return $this->devValue;
    }

    public function setDevValue(string $devValue): static
    {
        $this->devValue = $devValue;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getPackage(): ?Package
    {
        return $this->package;
    }

    public function setPackage(?Package $package): static
    {
        $this->package = $package;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAggregator(): ?Aggregator
    {
        return $this->aggregator;
    }

    public function setAggregator(?Aggregator $aggregator): static
    {
        $this->aggregator = $aggregator;

        return $this;
    }

    public function isRecurrent(): ?bool
    {
        return $this->recurrent;
    }

    public function setRecurrent(bool $recurrent): static
    {
        $this->recurrent = $recurrent;

        return $this;
    }
}
