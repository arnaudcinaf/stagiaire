<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Controller\Api\UserMeController;
use App\Core\Helpers\OpenApi\Helpers;
use App\Core\Route\UserRoute;
use App\Repository\UserRepository;
use App\State\Processor\UserStateProcessor;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: UserRoute::ITEM['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            security: Helpers::GRANTED_ADMIN,
            name: UserRoute::ITEM['name']
        ),
        new Get(
            uriTemplate: UserRoute::ME['path'],
            controller: UserMeController::class,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            security: Helpers::GRANTED_USER,
            read: false,
            name: UserRoute::ME['name']
        ),
        new Post(
            uriTemplate: UserRoute::ADD['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            security: Helpers::GRANTED_PUBLIC,
            name: UserRoute::ADD['name']
        ),
        new GetCollection(
            uriTemplate: UserRoute::COLLECTION['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            security: Helpers::GRANTED_ADMIN,
            name: UserRoute::COLLECTION['name']
        ),
        new Patch(
            uriTemplate: UserRoute::PATCH['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_UPDATE]],
            security: Helpers::GRANTED_USER,
            name: UserRoute::PATCH['name']
        )
    ],processor: UserStateProcessor::class
)]
class User implements PasswordAuthenticatedUserInterface, JWTUserInterface
{
    const GROUP_WRITE = 'user_write';
    const GROUP_READ = 'user_read';
    const GROUP_UPDATE = 'user_update';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $lastname = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $firstname = null;

    #[ORM\Column(length: 1)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $gender = null;

    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private array $roles = [];

    /**
     * @var null|string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[Groups([self::GROUP_WRITE, self::GROUP_UPDATE])]
    #[Assert\NotBlank(groups: [self::GROUP_WRITE])]
    #[Assert\NotCompromisedPassword(groups: [self::GROUP_WRITE, self::GROUP_UPDATE])]
    #[Assert\Regex(
        pattern: "/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[-+!*#$\ \@%_])([-+!*#$\ \@%_\w]{8,100})$/",
        message: "Votre mot de passe doit avoir au moins 8 caractères comprenant un chiffre, une lettre majuscule, une lettre minuscule, et un caractère spécial parmi [@, -, _, , $, %, !, *]",
        groups: [self::GROUP_WRITE, self::GROUP_UPDATE],
    )]
    #[SerializedName("password")]
    private ?string $plainPassword = null;

    private ?string $lastPassword = null;

    #[Groups([self::GROUP_WRITE, self::GROUP_UPDATE])]
    #[Assert\NotBlank(groups: [self::GROUP_WRITE])]
    #[Assert\EqualTo(
        propertyPath: 'plainPassword',
        message: "les mots de passe doivent être identiques",
        groups: [self::GROUP_WRITE, self::GROUP_UPDATE]
    )]
    private ?string $confirmPassword = null;

    #[ORM\Column]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?DateTimeImmutable $updatedAt = null;

    /**
     * @var Collection<int, Email>
     */
    #[ORM\OneToMany(targetEntity: Email::class, mappedBy: 'owner', orphanRemoval: true)]
    private Collection $emails;

    /**
     * @var Collection<int, Phone>
     */
    #[ORM\OneToMany(targetEntity: Phone::class, mappedBy: 'owner', orphanRemoval: true)]
    private Collection $phones;

    /**
     * @var Collection<int, Transaction>
     */
    #[ORM\OneToMany(targetEntity: Transaction::class, mappedBy: 'owner', orphanRemoval: true)]
    private Collection $transactions;

    /**
     * @var Collection<int, Subscription>
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'owner', orphanRemoval: true)]
    private Collection $subscriptions;

    /**
     * @var Collection<int, Customer>
     */
    #[ORM\OneToMany(targetEntity: Customer::class, mappedBy: 'owner', orphanRemoval: true)]
    private Collection $customers;

    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $identifier = null;

    public function __construct()
    {
        $this->emails = new ArrayCollection();
        $this->phones = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->customers = new ArrayCollection();

        $date = new DateTimeImmutable();

        $this->setCreatedAt($date)->setUpdatedAt($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     * @return User
     */
    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastPassword(): ?string
    {
        return $this->lastPassword;
    }

    /**
     * @param string|null $lastPassword
     * @return User
     */
    public function setLastPassword(?string $lastPassword): User
    {
        $this->lastPassword = $lastPassword;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getConfirmPassword(): ?string
    {
        return $this->confirmPassword;
    }

    /**
     * @param string|null $confirmPassword
     * @return User
     */
    public function setConfirmPassword(?string $confirmPassword): User
    {
        $this->confirmPassword = $confirmPassword;
        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Email>
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(Email $email): static
    {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
            $email->setOwner($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): static
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getOwner() === $this) {
                $email->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Phone>
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(Phone $phone): static
    {
        if (!$this->phones->contains($phone)) {
            $this->phones->add($phone);
            $phone->setOwner($this);
        }

        return $this;
    }

    public function removePhone(Phone $phone): static
    {
        if ($this->phones->removeElement($phone)) {
            // set the owning side to null (unless already changed)
            if ($phone->getOwner() === $this) {
                $phone->setOwner(null);
            }
        }

        return $this;
    }

    public static function createFromPayload($username, array $payload)
    {
        // TODO: Implement createFromPayload() method.

        return (new self())->setId((int) $payload['id'])->setRoles($payload['roles'] ?? []);
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        // TODO: Implement getRoles() method.
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
        $this->confirmPassword = null;
        $this->identifier = null;
    }

    public function getUserIdentifier(): string
    {
        // TODO: Implement getUserIdentifier() method.

        return (string)$this->id;
    }

    /**
     * @return Collection<int, Transaction>
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $transaction): static
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions->add($transaction);
            $transaction->setOwner($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $transaction): static
    {
        if ($this->transactions->removeElement($transaction)) {
            // set the owning side to null (unless already changed)
            if ($transaction->getOwner() === $this) {
                $transaction->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setOwner($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getOwner() === $this) {
                $subscription->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): static
    {
        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
            $customer->setOwner($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): static
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getOwner() === $this) {
                $customer->setOwner(null);
            }
        }

        return $this;
    }

    public function getDefaultEmail():string
    {
        $emails = $this->getEmails()->toArray();

        $defaultEmails = array_filter([...$emails], function (Email $email){
            return $email->isActive();
        });

        return $defaultEmails[0]->getValue();
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier ?? $this->getEmail() ?? $this->getPhone();
    }

    /**
     * @param string|null $identifier
     * @return User
     */
    public function setIdentifier(?string $identifier): User
    {
        $this->identifier = $identifier;
        return $this;
    }

    private function getEmail():?string
    {
        $emails = $this->getEmails()->toArray();

        return count($emails) > 0 ? $emails[0]->getValue() : null;
    }

    private function getPhone():?string
    {
        $phones = $this->getPhones()->toArray();

        return count($phones) > 0 ? $phones[0]->getValue() : null;
    }
}
