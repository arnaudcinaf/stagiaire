<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Core\Helpers\OpenApi\Helpers;
use App\Core\Route\AggregatorRoute;
use App\Repository\AggregatorRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AggregatorRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: AggregatorRoute::ITEM['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: AggregatorRoute::ITEM['name']
        ),
        new GetCollection(
            uriTemplate: AggregatorRoute::COLLECTION['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            security: Helpers::GRANTED_PUBLIC,
            name: AggregatorRoute::COLLECTION['name']
        ),
        new Post(
            uriTemplate: AggregatorRoute::ADD['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            name: AggregatorRoute::ADD['name']
        ),
        new Patch(
            uriTemplate: AggregatorRoute::PATCH['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_UPDATE]],
            name: AggregatorRoute::PATCH['name'],
        )
    ],
    security: Helpers::GRANTED_ADMIN
)]
class Aggregator
{
    const GROUP_WRITE = 'aggregator_write';
    const GROUP_READ = 'aggregator_read';
    const GROUP_UPDATE = 'aggregator_update';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $devApiKey = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $devPublicKey = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $apiKey = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $publicKey = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $webhookKey = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $version = null;

    #[ORM\Column]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?DateTimeImmutable $updatedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE, self::GROUP_UPDATE])]
    private ?string $strategy = null;

    /**
     * @var Collection<int, MethodPaid>
     */
    #[ORM\OneToMany(targetEntity: MethodPaid::class, mappedBy: 'aggregator', orphanRemoval: true)]
    private Collection $methodPaids;

    /**
     * @var Collection<int, Price>
     */
    #[ORM\OneToMany(targetEntity: Price::class, mappedBy: 'aggregator', orphanRemoval: true)]
    private Collection $prices;

    /**
     * @var Collection<int, Customer>
     */
    #[ORM\OneToMany(targetEntity: Customer::class, mappedBy: 'aggregator')]
    private Collection $customers;

    public function __construct()
    {
        $this->methodPaids = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $date = new DateTimeImmutable();
        $this->setCreatedAt($date)->setUpdatedAt($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDevApiKey(): ?string
    {
        return $this->devApiKey;
    }

    public function setDevApiKey(string $devApiKey): static
    {
        $this->devApiKey= $devApiKey;

        return $this;
    }

    public function getDevPublicKey(): ?string
    {
        return $this->devPublicKey;
    }

    public function setDevPublicKey(?string $devPublicKey): static
    {
        $this->devPublicKey = $devPublicKey;
        return $this;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): static
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getPublicKey(): ?string
    {
        return $this->publicKey;
    }

    public function setPublicKey(?string $publicKey): static
    {
        $this->publicKey = $publicKey;
        return $this;
    }

    public function getWebhookKey(): ?string
    {
        return $this->webhookKey;
    }

    public function setWebhookKey(?string $webhookKey): static
    {
        $this->webhookKey = $webhookKey;
        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): static
    {
        $this->version = $version;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getStrategy(): ?string
    {
        return $this->strategy;
    }

    public function setStrategy(string $strategy): static
    {
        $this->strategy = $strategy;

        return $this;
    }

    /**
     * @return Collection<int, MethodPaid>
     */
    public function getMethodPaids(): Collection
    {
        return $this->methodPaids;
    }

    public function addMethodPaid(MethodPaid $methodPaid): static
    {
        if (!$this->methodPaids->contains($methodPaid)) {
            $this->methodPaids->add($methodPaid);
            $methodPaid->setAggregator($this);
        }

        return $this;
    }

    public function removeMethodPaid(MethodPaid $methodPaid): static
    {
        if ($this->methodPaids->removeElement($methodPaid)) {
            // set the owning side to null (unless already changed)
            if ($methodPaid->getAggregator() === $this) {
                $methodPaid->setAggregator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Price>
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(Price $price): static
    {
        if (!$this->prices->contains($price)) {
            $this->prices->add($price);
            $price->setAggregator($this);
        }

        return $this;
    }

    public function removePrice(Price $price): static
    {
        if ($this->prices->removeElement($price)) {
            // set the owning side to null (unless already changed)
            if ($price->getAggregator() === $this) {
                $price->setAggregator(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): static
    {
        if (!$this->customers->contains($customer)) {
            $this->customers->add($customer);
            $customer->setAggregator($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): static
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getAggregator() === $this) {
                $customer->setAggregator(null);
            }
        }

        return $this;
    }
}
