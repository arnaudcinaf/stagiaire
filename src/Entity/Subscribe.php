<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Core\ApiRoute\SubscribeRoute;
use App\State\Processor\SubscribeStateProcessor;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new Post(
            uriTemplate: SubscribeRoute::POST['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            validationContext: ['groups' => [self::GROUP_WRITE]],
            name: SubscribeRoute::POST['name'],
            processor: SubscribeStateProcessor::class
        )
    ]
)]
class Subscribe
{
    const GROUP_READ = 'subscribe_read';
    const GROUP_WRITE= 'subscribe_write';

    #[Groups([self::GROUP_READ])]
    private ?int $id = null;
    #[Groups([self::GROUP_WRITE])]
    private ?Package $package = null;
    #[Groups([self::GROUP_WRITE])]
    private ?MethodPaid $methodPaid = null;
    #[Groups([self::GROUP_WRITE, self::GROUP_READ])]
    private ?string $redirectUrl = null;

    private ?string $mode = null;

    private ?string $session = null;

    private ?User $owner = null;

    private ?Transaction $transaction;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Subscribe
     */
    public function setId(?int $id): static
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Package|null
     */
    public function getPackage(): ?Package
    {
        return $this->package;
    }

    /**
     * @param Package|null $package
     * @return Subscribe
     */
    public function setPackage(?Package $package): static
    {
        $this->package = $package;
        return $this;
    }

    /**
     * @return MethodPaid|null
     */
    public function getMethodPaid(): ?MethodPaid
    {
        return $this->methodPaid;
    }

    /**
     * @param MethodPaid|null $methodPaid
     * @return Subscribe
     */
    public function setMethodPaid(?MethodPaid $methodPaid): static
    {
        $this->methodPaid = $methodPaid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    /**
     * @param string|null $redirectUrl
     * @return Subscribe
     */
    public function setRedirectUrl(?string $redirectUrl): static
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMode(): ?string
    {
        return $this->mode;
    }

    /**
     * @param string|null $mode
     * @return Subscribe
     */
    public function setMode(?string $mode): static
    {
        $this->mode = $mode;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSession(): ?string
    {
        return $this->session;
    }

    /**
     * @param string|null $session
     * @return Subscribe
     */
    public function setSession(?string $session): static
    {
        $this->session = $session;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @param User|null $owner
     * @return Subscribe
     */
    public function setOwner(?User $owner): static
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return Transaction|null
     */
    public function getTransaction(): ?Transaction
    {
        return $this->transaction;
    }

    /**
     * @param Transaction|null $transaction
     * @return Subscribe
     */
    public function setTransaction(?Transaction $transaction): static
    {
        $this->transaction = $transaction;
        return $this;
    }
}