<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Core\Helpers\OpenApi\Helpers;
use App\Core\Route\PackageRoute;
use App\Repository\PackageRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PackageRepository::class)]
#[ApiResource(
    operations: [
        new Get(
            uriTemplate: PackageRoute::ITEM['path'],
            requirements: Helpers::REQUIREMENT_ID,
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: PackageRoute::ITEM['name']
        ),
        new GetCollection(
            uriTemplate: PackageRoute::COLLECTION['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            name: PackageRoute::COLLECTION['name']
        ),
        new Post(
            uriTemplate: PackageRoute::ADD['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            security: Helpers::GRANTED_ADMIN,
            name: PackageRoute::ADD['name']
        ),
        new Patch(
            uriTemplate: PackageRoute::PATCH['path'],
            normalizationContext: ['groups' => [self::GROUP_READ]],
            denormalizationContext: ['groups' => [self::GROUP_WRITE]],
            security: Helpers::GRANTED_ADMIN,
            name: PackageRoute::PATCH['name']
        )
    ]
)]
class Package
{
    const GROUP_READ = 'package_read';
    const GROUP_WRITE = 'package_write';
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?float $price = null;

    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    #[Groups([self::GROUP_READ])]
    private ?DateTimeImmutable $updatedAt = null;

    /**
     * @var Collection<int, Subscription>
     */
    #[ORM\OneToMany(targetEntity: Subscription::class, mappedBy: 'package')]
    private Collection $subscriptions;

    #[ORM\Column(type: Types::SMALLINT)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?int $duration = null;

    /**
     * @var Collection<int, Price>
     */
    #[ORM\OneToMany(targetEntity: Price::class, mappedBy: 'package')]
    private Collection $prices;

    /**
     * @var Collection<int, Webhook>
     */
    #[ORM\OneToMany(targetEntity: Webhook::class, mappedBy: 'package')]
    private Collection $webhooks;

    #[ORM\Column(length: 255)]
    #[Groups([self::GROUP_READ, self::GROUP_WRITE])]
    private ?string $type = null;

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
        $this->prices = new ArrayCollection();
        $this->webhooks = new ArrayCollection();
        $date = new DateTimeImmutable();
        $this->setCreatedAt($date)->setUpdatedAt($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Subscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(Subscription $subscription): static
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setPackage($this);
        }

        return $this;
    }

    public function removeSubscription(Subscription $subscription): static
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getPackage() === $this) {
                $subscription->setPackage(null);
            }
        }

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): static
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @return Collection<int, Price>
     */
    public function getPrices(): Collection
    {
        return $this->prices;
    }

    public function addPrice(Price $price): static
    {
        if (!$this->prices->contains($price)) {
            $this->prices->add($price);
            $price->setPackage($this);
        }

        return $this;
    }

    public function removePrice(Price $price): static
    {
        if ($this->prices->removeElement($price)) {
            // set the owning side to null (unless already changed)
            if ($price->getPackage() === $this) {
                $price->setPackage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Webhook>
     */
    public function getWebhooks(): Collection
    {
        return $this->webhooks;
    }

    public function addWebhook(Webhook $webhook): static
    {
        if (!$this->webhooks->contains($webhook)) {
            $this->webhooks->add($webhook);
            $webhook->setPackage($this);
        }

        return $this;
    }

    public function removeWebhook(Webhook $webhook): static
    {
        if ($this->webhooks->removeElement($webhook)) {
            // set the owning side to null (unless already changed)
            if ($webhook->getPackage() === $this) {
                $webhook->setPackage(null);
            }
        }

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;

        return $this;
    }
}
