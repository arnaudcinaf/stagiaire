<?php

namespace App\Controller\Webhook;

use App\Core\Useful\Strategy;
use App\Service\Payment\SubscribeService;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\SignatureVerificationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class HandleWebhookController extends AbstractController
{
    public function __construct(private readonly SubscribeService $service)
    {
    }

    /**
     * @throws SignatureVerificationException
     * @throws ApiErrorException
     */
    public function handleStripe(): JsonResponse
    {
        $this->service->handleWebhook(strategy: Strategy::STRIPE);

        return $this->json(['status' => 'ok']);
    }
}