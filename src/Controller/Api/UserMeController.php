<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\User\UserInterface;

class UserMeController extends AbstractController
{
    public function __construct(private readonly UserRepository $repository)
    {
    }

    public function __invoke(): ?UserInterface
    {
        /** @var User|null $user */
        $user = $this->getUser();
        if ($user instanceof User) {
            /** @var User $user */
            $user = $this->repository->find($user->getId());
        }
        return $user;
    }
}