<?php

namespace App\Repository;

use App\Entity\Aggregator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Aggregator>
 *
 * @method Aggregator|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aggregator|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aggregator[]    findAll()
 * @method Aggregator[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AggregatorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aggregator::class);
    }

    //    /**
    //     * @return Aggregator[] Returns an array of Aggregator objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('a.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Aggregator
    //    {
    //        return $this->createQueryBuilder('a')
    //            ->andWhere('a.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
