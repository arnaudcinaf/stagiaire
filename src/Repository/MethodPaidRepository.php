<?php

namespace App\Repository;

use App\Entity\MethodPaid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MethodPaid>
 *
 * @method MethodPaid|null find($id, $lockMode = null, $lockVersion = null)
 * @method MethodPaid|null findOneBy(array $criteria, array $orderBy = null)
 * @method MethodPaid[]    findAll()
 * @method MethodPaid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MethodPaidRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MethodPaid::class);
    }

    //    /**
    //     * @return MethodPaid[] Returns an array of MethodPaid objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?MethodPaid
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
